package com.example.jsonken.fondmemorybook;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.example.jsonken.fondmemorybook.fragments.AddMemoryFragment;
import com.example.jsonken.fondmemorybook.objects.DatabaseHelper;
import com.example.jsonken.fondmemorybook.objects.Memory;
import com.example.jsonken.fondmemorybook.objects.MemoryListener;
import com.example.jsonken.fondmemorybook.objects.ScrapBook;
import com.example.jsonken.fondmemorybook.objects.ScrapbookHelper;
import com.example.jsonken.fondmemorybook.objects.ThemeHelper;

public class AddMemoryActivity extends AppCompatActivity implements MemoryListener {

    public static final String ADD_MEMORY_REQUEST =
            "com.example.jsonken.fondmemorybook.ADD_MEMORY_REQUEST";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ThemeHelper helper = new ThemeHelper();
        helper.setTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_layout);

        Intent startIntent = getIntent();
        String photoPath = startIntent.getStringExtra(ADD_MEMORY_REQUEST);

        if (savedInstanceState == null) {

            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container,
                            AddMemoryFragment.newInstance(photoPath), AddMemoryFragment.TAG)
                    .commit();
        }

        FrameLayout layout = findViewById(R.id.fragment_container);

        BitmapDrawable background = helper.setWallpaper(this);

        layout.setBackground(background);
    }

    @Override
    public void addMemory(Memory _memory) {
        ScrapbookHelper mHelper = ScrapbookHelper.getInstance();
        ScrapBook scrapBook = mHelper.getScrapbook();

        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this);
        databaseHelper.insertMemory(scrapBook.getTitle(), _memory.getmTitle(),
                _memory.getmDetails(), _memory.getmPhotoPath());

        Intent intent = getIntent();
        this.setResult(RESULT_OK, intent);

        finish();
    }
}
