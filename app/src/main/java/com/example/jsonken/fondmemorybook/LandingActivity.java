package com.example.jsonken.fondmemorybook;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class LandingActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_page);

        findViewById(R.id.button_guest).setOnClickListener(this);
        findViewById(R.id.button_create).setOnClickListener(this);
        findViewById(R.id.button_sign_in).setOnClickListener(this);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;

        ImageView handsImage = findViewById(R.id.imageView_hands);
        Bitmap hands =
                BitmapFactory.decodeResource(this.getResources(), R.drawable.baby_hand, options);
        handsImage.setImageBitmap(hands);

        ImageView feetImage = findViewById(R.id.imageView_feet);
        Bitmap feet =
                BitmapFactory.decodeResource(this.getResources(), R.drawable.beach_feet, options);
        feetImage.setImageBitmap(feet);

        ImageView cribImage = findViewById(R.id.imageView_crib);
        Bitmap crib =
                BitmapFactory.decodeResource(this.getResources(), R.drawable.crib, options);
        cribImage.setImageBitmap(crib);

        RelativeLayout layout = findViewById(R.id.layout_landing_page);

        Bitmap wallpaper =
                BitmapFactory.decodeResource(getResources(), R.drawable.wallpaper_light, options);

        BitmapDrawable background = new BitmapDrawable(getResources(), wallpaper);

        layout.setBackground(background);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_create:
                Toast.makeText(this, "Click continue as guest", Toast.LENGTH_SHORT).show();
                return;
            case R.id.button_sign_in:
                Toast.makeText(this, "Click continue as guest", Toast.LENGTH_SHORT).show();
                return;
            case R.id.button_guest:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
        }
    }
}
