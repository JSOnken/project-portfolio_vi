package com.example.jsonken.fondmemorybook;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.example.jsonken.fondmemorybook.fragments.ConfigFragment;
import com.example.jsonken.fondmemorybook.fragments.MemoryGridFragment;
import com.example.jsonken.fondmemorybook.fragments.ScrapbookListFragment;
import com.example.jsonken.fondmemorybook.objects.ConfigSaveListener;
import com.example.jsonken.fondmemorybook.objects.Memory;
import com.example.jsonken.fondmemorybook.objects.MemoryListListener;
import com.example.jsonken.fondmemorybook.objects.ScrapBook;
import com.example.jsonken.fondmemorybook.objects.ScrapbookHelper;
import com.example.jsonken.fondmemorybook.objects.ScrapbookListListener;
import com.example.jsonken.fondmemorybook.objects.ThemeHelper;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        MemoryListListener, ConfigSaveListener, ScrapbookListListener {

    private static final int REQUEST_TAKE_PICTURE = 0x01010;
    private FloatingActionButton fab;
    private RelativeLayout mLayout;
    private BottomNavigationView mNavigation;
    private ThemeHelper mHelper;
    private String mTheme;
    private ScrapBook mScrapbook;
    private String mTimestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mHelper = new ThemeHelper();
        mHelper.setTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ScrapbookHelper helper = ScrapbookHelper.getInstance();
        helper.setScrapbookArrayList(loadScrapbooks());

        mLayout = findViewById(R.id.container);

        BitmapDrawable background = mHelper.setWallpaper(this);

        mLayout.setBackground(background);

        handleIntent(getIntent());

        mTheme = mHelper.getTheme(this);

        fab = findViewById(R.id.fab_camera);
        fab.setOnClickListener(this);
        mNavigation = findViewById(R.id.navigation);
        mNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (mTheme.toLowerCase().equals("list item 2")) {
            mNavigation.setBackgroundColor(getColor(R.color.grayColorPrimaryDark));
            fab.setImageDrawable(getDrawable(R.drawable.camera_dark));
        }

        ScrapbookHelper mScrapbookHelper = ScrapbookHelper.getInstance();
        mScrapbook = mScrapbookHelper.getScrapbook();

        if (mScrapbook == null) {
            mNavigation.getMenu().findItem(R.id.navigation_home).setEnabled(false);
            mNavigation.setSelectedItemId(R.id.navigation_albums);
        } else {
            setTitle(mScrapbook.getTitle());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mScrapbook != null) {
            mNavigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        if (mTheme.toLowerCase().equals("list item 2")) {
            MenuItem searchIcon = menu.findItem(R.id.action_search);
            searchIcon.setIcon(getDrawable(R.drawable.search_dark));
        }

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
        }
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    BitmapDrawable background = mHelper.setWallpaper(getApplicationContext());

                    mLayout.setBackground(background);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.navigation_fragment_container,
                                    MemoryGridFragment.newInstance(), MemoryGridFragment.TAG)
                            .commit();

                    setTitle(mScrapbook.getTitle());
                    fab.show();

                    return true;
                case R.id.navigation_albums:
                    loadScrapbooks();
                    background = mHelper.setWallpaper(getApplicationContext());

                    mLayout.setBackground(background);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.navigation_fragment_container,
                                    ScrapbookListFragment.newInstance(mTheme),
                                    ScrapbookListFragment.TAG)
                            .commit();

                    setTitle("Scrapbooks");
                    fab.hide();

                    return true;
                case R.id.navigation_settings:
                    mLayout.setBackgroundColor(getColor(R.color.white));

                    getFragmentManager().beginTransaction()
                            .replace(R.id.navigation_fragment_container,
                                    ConfigFragment.newInstance(mTheme), ConfigFragment.TAG)
                            .commit();

                    setTitle("Settings");
                    fab.hide();

                    return true;
            }
            return false;
        }
    };

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab_camera) {
            mTimestamp = getTimestampString();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputUri());
            startActivityForResult(intent, REQUEST_TAKE_PICTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String photoPath = getOutputFilePath();
        Intent intent = new Intent(this, AddMemoryActivity.class);
        intent.putExtra(AddMemoryActivity.ADD_MEMORY_REQUEST, photoPath);
        startActivity(intent);
    }

    private Uri getOutputUri() {
        File storageDir = getExternalFilesDir("images");
        File imageFile = new File(storageDir, "photoBook" + mTimestamp + ".jpg");
        try {
            imageFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return FileProvider.getUriForFile(this, getString(R.string.fileProvider), imageFile);
    }

    private String getOutputFilePath() {
        File protectedStorage = getExternalFilesDir("images");
        File imageFile = new File(protectedStorage, "photoBook" + mTimestamp + ".jpg");
        try {
            imageFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageFile.getAbsolutePath();
    }

    @Override
    public void getSelectedMemory(Memory memory) {
        Intent intent = new Intent(this, SelectedMemoryActivity.class);
        intent.putExtra(SelectedMemoryActivity.SELECT_MEMORY_REQUEST, memory);
        startActivity(intent);
    }

    @Override
    public void updateTheme() {
        recreate();
        if (mScrapbook != null) {
            mNavigation.setSelectedItemId(R.id.navigation_home);
        } else {
            mNavigation.setSelectedItemId(R.id.navigation_albums);
        }
    }

    @Override
    public void getSelectedScrapbook(int position) {
        ScrapbookHelper helper = ScrapbookHelper.getInstance();
        mScrapbook = helper.getSelectedScrapbook(position);
        helper.setScrapbook(mScrapbook);
        mNavigation.getMenu().findItem(R.id.navigation_home).setEnabled(true);
        mNavigation.setSelectedItemId(R.id.navigation_home);
        recreate();
    }

    private String getTimestampString() {
        Long tempTS = System.currentTimeMillis()/1000;

        return tempTS.toString();
    }

    private ArrayList<ScrapBook> loadScrapbooks() {
        ArrayList<ScrapBook> scrapbooksList = new ArrayList<>();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String json = prefs.getString("SCRAPBOOKS", null);

        if (json != null) {
            try {
                JSONArray array = new JSONArray(json);
                for (int i = 0; i < array.length(); i++) {
                    String name = array.optString(i);
                    ScrapBook scrapBook = new ScrapBook(name);
                    scrapbooksList.add(scrapBook);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return scrapbooksList;
    }
}
