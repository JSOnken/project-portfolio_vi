package com.example.jsonken.fondmemorybook;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.example.jsonken.fondmemorybook.fragments.SelectedMemoryFragment;
import com.example.jsonken.fondmemorybook.objects.Memory;
import com.example.jsonken.fondmemorybook.objects.ThemeHelper;

public class SelectedMemoryActivity extends AppCompatActivity {

    public static final String SELECT_MEMORY_REQUEST =
            "com.example.jsonken.fondmemorybook.SELECT_MEMORY_REQUEST";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ThemeHelper helper = new ThemeHelper();
        helper.setTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_layout);

        Intent startIntent = getIntent();
        Memory memory = (Memory) startIntent.getSerializableExtra(SELECT_MEMORY_REQUEST);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, SelectedMemoryFragment.newInstance(memory),
                            SelectedMemoryFragment.TAG)
                    .commit();
        }

        FrameLayout layout = findViewById(R.id.fragment_container);

        BitmapDrawable background = helper.setWallpaper(this);

        layout.setBackground(background);
    }
}
