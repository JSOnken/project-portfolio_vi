package com.example.jsonken.fondmemorybook.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jsonken.fondmemorybook.objects.DatabaseHelper;
import com.example.jsonken.fondmemorybook.R;
import com.example.jsonken.fondmemorybook.objects.ThemeHelper;

public class MemoryCursorAdapter extends CursorAdapter {

    public MemoryCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.memory_adapter_layout, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ThemeHelper helper = new ThemeHelper();
        String theme = helper.getTheme(context);
        int backgroundColor = helper.getBackgroundColor(context);
        LinearLayout layout = view.findViewById(R.id.adapter_layout);
        layout.setBackgroundColor(backgroundColor);

        String title =
                cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MEMORY_TITLE));
        String details =
                cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MEMORY_DETAILS));
        String photoPath =
                cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MEMORY_PHOTO_PATH));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 10;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath);

        ImageView imageView = view.findViewById(R.id.imageView_memory_photo);
        imageView.setImageBitmap(bitmap);

        TextView tvTitle = view.findViewById(R.id.textView_memory_title);
        tvTitle.setText(title);

        TextView tvDetails = view.findViewById(R.id.textView_memory_details);
        tvDetails.setText(details);

        if (theme.toLowerCase().equals("list item 2")) {
            tvTitle.setTextColor(context.getColor(R.color.darkGray));
            tvDetails.setTextColor(context.getColor(R.color.darkGray));
        }
    }
}
