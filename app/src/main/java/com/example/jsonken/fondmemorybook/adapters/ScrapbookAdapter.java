package com.example.jsonken.fondmemorybook.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jsonken.fondmemorybook.R;
import com.example.jsonken.fondmemorybook.objects.ScrapBook;
import com.example.jsonken.fondmemorybook.objects.ScrapbookHelper;

import java.util.ArrayList;

public class ScrapbookAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<ScrapBook> mScrapbookArrayList;

    public ScrapbookAdapter(Context _context) {
        this.mContext = _context;

        ScrapbookHelper helper = ScrapbookHelper.getInstance();
        mScrapbookArrayList = helper.getScrapbooks();
    }

    @Override
    public int getCount() {
        return mScrapbookArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mScrapbookArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_adapter_layout, null);
        }

        TextView textView = convertView.findViewById(R.id.textView_list_adapter);
        textView.setText(mScrapbookArrayList.get(position).getTitle());

        return convertView;
    }
}
