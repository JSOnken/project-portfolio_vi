package com.example.jsonken.fondmemorybook.fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jsonken.fondmemorybook.R;
import com.example.jsonken.fondmemorybook.objects.Memory;
import com.example.jsonken.fondmemorybook.objects.MemoryListener;
import com.example.jsonken.fondmemorybook.objects.ThemeHelper;

public class AddMemoryFragment extends Fragment {

    public static final String TAG = "AddMemoryFragment.TAG";
    private static String mPhotoPath;
    private MemoryListener mListener;

    public static AddMemoryFragment newInstance(String _photoPath) {

        Bundle args = new Bundle();

        mPhotoPath = _photoPath;
        AddMemoryFragment fragment = new AddMemoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        ThemeHelper helper = new ThemeHelper();
        String theme = helper.getTheme(getContext());

        View view = inflater.inflate(R.layout.memory_fragment, null);

        EditText title = view.findViewById(R.id.editText_title);
        EditText details = view.findViewById(R.id.editText_details);

        if (theme.toLowerCase().equals("list item 2")) {
            title.setHintTextColor(getContext().getColor(R.color.lightText));
            title.setTextColor(getContext().getColor(R.color.white));
            details.setHintTextColor(getContext().getColor(R.color.lightText));
            details.setTextColor(getContext().getColor(R.color.white));
        }

        ImageView photoImage = view.findViewById(R.id.imageView_memory);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;

        Bitmap bitmap = BitmapFactory.decodeFile(mPhotoPath, options);
        photoImage.setImageBitmap(bitmap);

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MemoryListener) {
            mListener = (MemoryListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "MemoryListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        ThemeHelper helper = new ThemeHelper();
        String theme = helper.getTheme(getContext());

        if (theme.toLowerCase().equals("list item 2")) {
            MenuItem saveItem = menu.findItem(R.id.action_save);
            saveItem.setIcon(getContext().getDrawable(R.drawable.save_dark));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        EditText title = getView().findViewById(R.id.editText_title);
        EditText details = getView().findViewById(R.id.editText_details);

        String titleString = title.getText().toString();
        String detailsString = details.getText().toString();

        if (titleString.trim().equals("") || detailsString.trim().equals("")) {
            Toast.makeText(getContext(), "All fields must have values", Toast.LENGTH_SHORT).show();
        } else {
            Memory newMemory = new Memory(titleString, detailsString, mPhotoPath);
            mListener.addMemory(newMemory);
        }

        return true;
    }
}
