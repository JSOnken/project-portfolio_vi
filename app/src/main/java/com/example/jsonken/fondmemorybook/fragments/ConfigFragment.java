package com.example.jsonken.fondmemorybook.fragments;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.jsonken.fondmemorybook.R;
import com.example.jsonken.fondmemorybook.objects.ConfigSaveListener;

public class ConfigFragment extends PreferenceFragment {

    public static final String TAG = "ConfigFragment.TAG";
    private ConfigSaveListener mListener;
    private static String mTheme;

    public static ConfigFragment newInstance(String _theme) {
        mTheme = _theme;

        return new ConfigFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs_config);

        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ConfigSaveListener) {
            mListener = (ConfigSaveListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "ConfigSaveListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(false);

        if (mTheme.toLowerCase().equals("list item 2")) {
            MenuItem saveItem = menu.findItem(R.id.action_save);
            saveItem.setIcon(getActivity().getDrawable(R.drawable.save_dark));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mListener.updateTheme();

        return true;
    }
}
