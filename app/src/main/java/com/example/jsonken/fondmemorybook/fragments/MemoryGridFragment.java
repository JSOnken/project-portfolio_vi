package com.example.jsonken.fondmemorybook.fragments;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.jsonken.fondmemorybook.objects.DatabaseHelper;
import com.example.jsonken.fondmemorybook.R;
import com.example.jsonken.fondmemorybook.adapters.MemoryCursorAdapter;
import com.example.jsonken.fondmemorybook.objects.Memory;
import com.example.jsonken.fondmemorybook.objects.MemoryListListener;

public class MemoryGridFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final String TAG = "MemoryGridFragment.TAG";
    private MemoryListListener mListener;

    public static MemoryGridFragment newInstance() {

        Bundle args = new Bundle();

        MemoryGridFragment fragment = new MemoryGridFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        DatabaseHelper helper = DatabaseHelper.getInstance(getActivity());

        View view = inflater.inflate(R.layout.album_fragment, null);

        GridView gridView = view.findViewById(R.id.gridView_album);

        MemoryCursorAdapter adapter = new MemoryCursorAdapter(getActivity(), helper.getAllData());

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MemoryListListener) {
            mListener = (MemoryListListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "MemoryListListener");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor c = (Cursor) parent.getAdapter().getItem(position);

        String title = c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MEMORY_TITLE));
        String details = c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MEMORY_DETAILS));
        String photoPath = c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MEMORY_PHOTO_PATH));

        Memory memory = new Memory(title, details, photoPath);
        mListener.getSelectedMemory(memory);
    }
}
