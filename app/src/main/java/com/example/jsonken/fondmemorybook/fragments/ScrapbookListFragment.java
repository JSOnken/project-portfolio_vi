package com.example.jsonken.fondmemorybook.fragments;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jsonken.fondmemorybook.R;
import com.example.jsonken.fondmemorybook.adapters.ScrapbookAdapter;
import com.example.jsonken.fondmemorybook.objects.ScrapbookHelper;
import com.example.jsonken.fondmemorybook.objects.ScrapBook;
import com.example.jsonken.fondmemorybook.objects.MemoryListListener;
import com.example.jsonken.fondmemorybook.objects.ScrapbookListListener;

import org.json.JSONArray;

import java.util.ArrayList;

public class ScrapbookListFragment extends ListFragment {

    public static final String TAG = "ScrapbookListFragment.TAG";
    private static String mTheme;
    private ScrapbookAdapter mAdapter;
    private ScrapbookListListener mListener;

    public static ScrapbookListFragment newInstance(String _theme) {
        mTheme = _theme;

        Bundle args = new Bundle();

        ScrapbookListFragment fragment = new ScrapbookListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.album_list_fragment, container, false);

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new ScrapbookAdapter(getActivity());
        setListAdapter(mAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MemoryListListener) {
            mListener = (ScrapbookListListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "ScrapbookListListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(false);

        if (mTheme.toLowerCase().equals("list item 2")) {
            MenuItem saveItem = menu.findItem(R.id.action_add);
            saveItem.setIcon(getActivity().getDrawable(R.drawable.add_dark));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        createAlertDialog();

        return true;
    }

    private void createAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertView = inflater.inflate(R.layout.alert_layout, null);
        builder.setView(alertView);

        final EditText editText = alertView.findViewById(R.id.editText_alert);

        builder.setTitle("Scrapbook Name");
        builder.setMessage("Enter a name for your scrapbook");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ScrapbookHelper helper = ScrapbookHelper.getInstance();
                String albumName = editText.getText().toString();
                if (albumName.trim().equals("")) {
                    Toast.makeText(getActivity(), "You must enter a name", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    String title = editText.getText().toString();
                    ScrapBook scrapBook = new ScrapBook(title);
                    helper.addScrapbook(scrapBook);
                    saveScrapbooks();
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        mListener.getSelectedScrapbook(position);
    }

    private void saveScrapbooks() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = prefs.edit();

        JSONArray array = new JSONArray();

        ScrapbookHelper helper = ScrapbookHelper.getInstance();
        ArrayList<ScrapBook> scrapbooks = helper.getScrapbooks();

        for (ScrapBook scrapBook : scrapbooks) {
            array.put(scrapBook.getTitle());
        }
        if (!scrapbooks.isEmpty()) {
            editor.putString("SCRAPBOOKS", array.toString());
        }
        editor.apply();
    }
}
