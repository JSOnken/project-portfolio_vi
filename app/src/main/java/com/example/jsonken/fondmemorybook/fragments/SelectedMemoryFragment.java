package com.example.jsonken.fondmemorybook.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jsonken.fondmemorybook.R;
import com.example.jsonken.fondmemorybook.objects.Memory;
import com.example.jsonken.fondmemorybook.objects.ThemeHelper;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

public class SelectedMemoryFragment extends Fragment {

    public static final String TAG = "SelectedMemoryFragment.TAG";
    private static int mPosition;
    private Bitmap mBitmap;
    private String mDetails;
    private ShareDialog mShareDialog;
    private static Memory mSelectedMemory;

    public static SelectedMemoryFragment newInstance(Memory _memory) {

        Bundle args = new Bundle();

        mSelectedMemory = _memory;

        SelectedMemoryFragment fragment = new SelectedMemoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        mShareDialog = new ShareDialog(getActivity());

        View view = inflater.inflate(R.layout.selected_memory_fragment, null);

        ThemeHelper themeHelper = new ThemeHelper();
        String theme = themeHelper.getTheme(getActivity());

        getActivity().setTitle(mSelectedMemory.getmTitle());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;

        mBitmap = BitmapFactory.decodeFile(mSelectedMemory.getmPhotoPath());

        ImageView photo = view.findViewById(R.id.imageView_selected_memory);
        photo.setImageBitmap(mBitmap);

        TextView details = view.findViewById(R.id.textView_selected_memory);
        mDetails = mSelectedMemory.getmDetails();
        details.setText(mDetails);

        if (theme.toLowerCase().equals("list item 2")) {
            details.setTextColor(getActivity().getColor(R.color.white));
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.share_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_facebook:

                if (mShareDialog.canShow(ShareLinkContent.class)) {

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(mBitmap)
                            .build();

                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();

                    mShareDialog.show(content);
                }
                break;
            case R.id.action_twitter:

                Uri uri = Uri.parse(mSelectedMemory.getmPhotoPath());

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, mDetails);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("image/jpg");
                shareIntent.setPackage("com.twitter.android");
                startActivity(Intent.createChooser(shareIntent, "Share Memory"));

                break;
        }

        return true;
    }
}
