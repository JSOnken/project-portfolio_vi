package com.example.jsonken.fondmemorybook.objects;

public interface ConfigSaveListener {
    void updateTheme();
}
