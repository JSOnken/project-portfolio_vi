package com.example.jsonken.fondmemorybook.objects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_FILE = "memoryDatabase.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "memories";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_SCRAPBOOK_NAME = "scrapbook_name";
    public static final String COLUMN_MEMORY_TITLE = "memory_title";
    public static final String COLUMN_MEMORY_DETAILS = "memory_details";
    public static final String COLUMN_MEMORY_PHOTO_PATH = "memory_photo_path";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_SCRAPBOOK_NAME + " TEXT, " +
            COLUMN_MEMORY_TITLE + " TEXT, " +
            COLUMN_MEMORY_DETAILS + " TEXT, " +
            COLUMN_MEMORY_PHOTO_PATH + " TEXT)";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static DatabaseHelper INSTANCE = null;

    public static DatabaseHelper getInstance(Context _context) {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseHelper(_context);
        }

        return INSTANCE;
    }

    private final SQLiteDatabase mDatabase;

    private DatabaseHelper(Context _context) {
        super(_context, DATABASE_FILE, null, DATABASE_VERSION);

        mDatabase = getWritableDatabase();
    }

    public void insertMemory(String _scrapbookName, String _memoryTitle, String _memoryDetails,
                             String _memoryPhotoPath) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SCRAPBOOK_NAME, _scrapbookName);
        values.put(COLUMN_MEMORY_TITLE, _memoryTitle);
        values.put(COLUMN_MEMORY_DETAILS, _memoryDetails);
        values.put(COLUMN_MEMORY_PHOTO_PATH, _memoryPhotoPath);

        mDatabase.insert(TABLE_NAME, null, values);
    }

    public Cursor getAllData() {
        return mDatabase.query(TABLE_NAME, null, null, null, null, null, null);
    }
}
