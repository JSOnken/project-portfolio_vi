package com.example.jsonken.fondmemorybook.objects;

import java.io.Serializable;

public class Memory implements Serializable {

    private final String mTitle;
    private final String mDetails;
    private final String mPhotoPath;

    public Memory(String _title, String _details, String _photoPath) {
        this.mTitle = _title;
        this.mDetails = _details;
        this.mPhotoPath = _photoPath;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmDetails() {
        return mDetails;
    }

    public String getmPhotoPath() {
        return mPhotoPath;
    }
}

