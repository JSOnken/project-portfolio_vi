package com.example.jsonken.fondmemorybook.objects;

public interface MemoryListListener {
    void getSelectedMemory(Memory memory);
}
