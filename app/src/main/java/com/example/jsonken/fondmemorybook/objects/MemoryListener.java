package com.example.jsonken.fondmemorybook.objects;

public interface MemoryListener {
    void addMemory(Memory _memory);
}
