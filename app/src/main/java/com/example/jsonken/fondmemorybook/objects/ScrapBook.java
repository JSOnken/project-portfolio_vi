package com.example.jsonken.fondmemorybook.objects;

import java.io.Serializable;

public class ScrapBook implements Serializable {

    private final String mTitle;

    public ScrapBook(String _title) {
        mTitle = _title;
    }

    public String getTitle() {
        return mTitle;
    }
}
