package com.example.jsonken.fondmemorybook.objects;

import java.util.ArrayList;

public class ScrapbookHelper {

    private static ScrapbookHelper mInstance;
    private static ArrayList<ScrapBook> mScrapbookArrayList = new ArrayList<>();
    private ScrapBook mScrapbook;

    private ScrapbookHelper() {
    }

    public static ScrapbookHelper getInstance() {
        if (mInstance == null) {
            mInstance = new ScrapbookHelper();
        }

        return mInstance;
    }

    public void addScrapbook(ScrapBook _scrapbook) {
        mScrapbookArrayList.add(_scrapbook);
    }

    public ArrayList<ScrapBook> getScrapbooks() {
        return mScrapbookArrayList;
    }

    public void setScrapbookArrayList(ArrayList<ScrapBook> _scrapbooks) {
        mScrapbookArrayList = _scrapbooks;
    }

    public ScrapBook getSelectedScrapbook(int _position) {
        return mScrapbookArrayList.get(_position);
    }

    public void setScrapbook(ScrapBook _scrapbook) {
        this.mScrapbook = _scrapbook;
    }

    public ScrapBook getScrapbook() {
        return mScrapbook;
    }
}
