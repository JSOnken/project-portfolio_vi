package com.example.jsonken.fondmemorybook.objects;

public interface ScrapbookListListener {
    void getSelectedScrapbook(int position);
}
