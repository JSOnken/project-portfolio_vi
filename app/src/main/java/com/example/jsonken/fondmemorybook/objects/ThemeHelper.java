package com.example.jsonken.fondmemorybook.objects;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;

import com.example.jsonken.fondmemorybook.R;

public class ThemeHelper {

    public ThemeHelper() {
    }

    public BitmapDrawable setWallpaper(Context _context) {

        BitmapDrawable background = null;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);
        String theme = preferences.getString(_context.getString(R.string.theme_key),
                _context.getString(R.string.default_theme));

        if (theme.toLowerCase().equals("list item 1")) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Bitmap wallpaper =
                    BitmapFactory.decodeResource(_context.getResources(), R.drawable
                            .wallpaper_light, options);

            background = new BitmapDrawable(_context.getResources(), wallpaper);
        } else if (theme.toLowerCase().equals("list item 2")) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Bitmap wallpaper =
                    BitmapFactory.decodeResource(_context.getResources(), R.drawable
                            .wallpaper_dark, options);

            background = new BitmapDrawable(_context.getResources(), wallpaper);
        }

        return background;
    }

    public void setTheme(Context _context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);
        String color = preferences.getString(_context.getString(R.string.accent_key),
                _context.getString(R.string.default_theme));
        String theme = preferences.getString(_context.getString(R.string.theme_key),
                _context.getString(R.string.default_theme));

        if (theme.toLowerCase().equals("list item 1")) {
            switch (color.toLowerCase()) {
                case "list item 1":
                    _context.setTheme(R.style.AppTheme);
                    return;
                case "list item 2":
                    _context.setTheme(R.style.AppTheme_Pink);
                    return;
                case "list item 3":
                    _context.setTheme(R.style.AppTheme_Blue);
                    return;
                case "list item 4":
                    _context.setTheme(R.style.AppTheme_Green);
                    return;
                case "list item 5":
                    _context.setTheme(R.style.AppTheme_Gray);
            }
        } else if (theme.toLowerCase().equals("list item 2")){
            switch (color.toLowerCase()) {
                case "list item 1":
                    _context.setTheme(R.style.DarkAppTheme);
                    return;
                case "list item 2":
                    _context.setTheme(R.style.DarkAppTheme_Pink);
                    return;
                case "list item 3":
                    _context.setTheme(R.style.DarkAppTheme_Blue);
                    return;
                case "list item 4":
                    _context.setTheme(R.style.DarkAppTheme_Green);
                    return;
                case "list item 5":
                    _context.setTheme(R.style.DarkAppTheme_Gray);
            }
        }
    }

    public String getTheme(Context _context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);

        return preferences.getString(_context.getString(R.string.theme_key),
                _context.getString(R.string.default_theme));
    }

    public int getBackgroundColor(Context _context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);
        String color = preferences.getString(_context.getString(R.string.accent_key),
                _context.getString(R.string.default_theme));

        int backgroundColor = -1;

        switch (color.toLowerCase()) {
            case "list item 1":
                backgroundColor = _context.getColor(R.color.colorPrimary);
                return backgroundColor;
            case "list item 2":
                backgroundColor = _context.getColor(R.color.pinkColorPrimary);
                return backgroundColor;
            case "list item 3":
                backgroundColor = _context.getColor(R.color.blueColorPrimary);
                return backgroundColor;
            case "list item 4":
                backgroundColor = _context.getColor(R.color.greenColorPrimary);
                return backgroundColor;
            case "list item 5":
                backgroundColor = _context.getColor(R.color.grayColorPrimary);
                return backgroundColor;
        }

        return backgroundColor;
    }
}
